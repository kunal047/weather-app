module.exports = {

    tableName: 'location_history',
    attributes: {
        id: {
            type: "string",
            columnName: "location_id",
            required: true
        },
        code: {
            type: "string",
            required: true
        },
        city: {
            type: "string",
            required: true
        },
        current_temp: {
            type: "string",
            required: true
        },
        temp_min: {
            type: "string",
            required: true
        },
        temp_max: {
            type: "string",
            required: true
        },
        created_at: {
            type: "string",
            required: true
        },

    }
}