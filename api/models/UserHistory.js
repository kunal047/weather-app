module.exports = {

    tableName: 'user_history',
    attributes: {
        id: {
            type: "string",
            columnName: "location_id",
            required: true
        },
        user_id: {
            type: "number",
            required: true
        },
    }
}