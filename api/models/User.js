module.exports = {

    tableName: 'users',
    attributes: {
        id: {
            type: "number",
            columnName: "user_id",
            required: true
        },
        name: {
            type: "string",
            required: true
        },
        mobile : {
            type: "string",
            required: true
        },
        email : {
            type: "string",
            required: true
        },
        city : {
            type: "string",
            required: true
        },
        country : {
            type: "string",
            required: true
        },

    }
}