
var request = require('request');
module.exports = {
    login : async (req, res, next) => {
        var email = req.param('email');
        sails.log(req.allParams());
        var user = await User.find({ email : email });
        if (user.length > 0 ) {
            req.session.userId = user[0]['id'];
            return res.send("ok");
        } else {
            return res.send("fail");
        }
    },
    logout: async (req, res, next) => {
        req.session.userId = '';
        return res.redirect("/");
    },

    register : async (req, res, next) => {
        sails.log(req.allParams());
        var count = await User.count({});
        var userCreated = await User.findOrCreate({ email : req.param('email') }, {
          id: count + 1,
          name: req.param("name"),
          mobile: req.param("mobile"),
          email: req.param("email"),
          city: req.param("city"),
          country: req.param("country")
        }).exec(async (err, user, wasCreated) => {
            if (err) { return res.send("fail"); }

            if (wasCreated) {
                // sails.log('Created a new user: ' + user.name);
                return res.send("ok");
            }
            else {
                // sails.log('Found existing user: ' + user.name);
                return res.send("exists");
            }
        });;
        
    },

    dashboard : async (req, res, next) => {

        const id = req.session.userId ;
        if (req.session.userId === undefined || id === "") {
            return res.redirect("/")
        }
        var user = await User.find({ id: id });
        await sails.sendNativeQuery(`SELECT user_history.user_id, location_history.city, location_history.current_temp, location_history.temp_min, location_history.temp_max, location_history.created_at FROM location_history INNER JOIN user_history ON user_history.location_id=location_history.location_id WHERE user_history.user_id=${id}`, (err, response) => {
            return res.view("pages/dashboard", { user : user[0], history : response.rows });
        });

    },

    weather : async (req, res, next) => {
        const id = req.session.userId || '';
        var location = req.param('location');
        
        if (req.session.userId === undefined || id == '' ) {
            return res.send("loggedout");
        } else if (location === '') {
            return res.send("fail");
        } else {
            request(`http://api.openweathermap.org/data/2.5/weather?q=${location}&appid=b70acabfa5f3787499dfcf0a4702a00a&units=metric`, async function (error, response, body) {
                if (error) {
                    return res.send("fail");
                }

                sails.log("body " + body);
                body = JSON.parse(body);
                sails.log("body " + body['cod']);

                if (body['cod'] === '404') {
                    return res.json({
                        current_temp: body["message"],
                        temp_min: body["message"],
                        temp_max: body["message"]
                    });
                } else {
                    var indianTimeZoneVal = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Kolkata'
                    });
                    var currentTime = new Date(indianTimeZoneVal).toLocaleString();
                    var locationID = `${Math.round(
                        new Date().valueOf() + Math.random()
                    )}`;
                    sails.log("location id " + locationID);
                    await LocationHistory.create({
                        id: locationID,
                        code: body["id"],
                        city: body["name"],
                        current_temp: body['main']['temp'],
                        temp_min: body['main']['temp_min'],
                        temp_max: body['main']['temp_max'],
                        created_at: currentTime
                    });
                    await UserHistory.create({
                        id: locationID,
                        user_id: req.session.userId,
                    });
                    return res.json({
                      city: body["name"],
                      current_temp: body["main"]["temp"],
                      temp_min: body["main"]["temp_min"],
                      temp_max: body["main"]["temp_max"],
                      created_at: currentTime
                    });
                }
            });
        }
    }
}